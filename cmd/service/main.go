package main

import (
	"flag"
	"net/http"
	"os"

	"github.com/go-kit/kit/log"

	httptransport "github.com/go-kit/kit/transport/http"

	blogonext "github.com/revas/blogo-next/pkg"

	// sdatastore "github.com/revas/blogo-next/pkg/service/datastore"
	sdatastore "github.com/revas/blogo-next/pkg/service/mockup"
	thttp "github.com/revas/blogo-next/pkg/transport/http"
)

func main() {
	var (
		listen = flag.String("listen", ":8080", "HTTP listen address")
	)
	flag.Parse()

	var logger log.Logger
	logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	logger = log.With(logger, "timestamp", log.DefaultTimestampUTC, "caller", log.DefaultCaller)

	var svc blogonext.ArticlesService

	svc = &sdatastore.ArticlesService{
		Logger: logger,
	}

	listArticlesHandler := httptransport.NewServer(
		thttp.MakeListArticlesEndpoint(svc),
		thttp.DecodeListArticlesRequest,
		thttp.EncodeResponse,
		httptransport.ServerErrorLogger(logger),
	)

	getArticleHandler := httptransport.NewServer(
		thttp.MakeGetArticleEndpoint(svc),
		thttp.DecodeGetArticleRequest,
		thttp.EncodeResponse,
		httptransport.ServerErrorLogger(logger),
	)

	http.Handle("/blogonext.ListArticles", listArticlesHandler)
	http.Handle("/blogonext.GetArticle", getArticleHandler)
	logger.Log("msg", "HTTP", "addr", *listen)
	logger.Log("err", http.ListenAndServe(*listen, nil))
}
