package blogonext

type ListArticlesRequest struct {
	Realm  string `json:"realm"`
	Domain string `json:"domain"`
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
}

type ListArticlesResponse struct {
	Articles []*Article `json:"articles"`
	Err      string     `json:"err,omitempty"`
}

type GetArticleRequest struct {
	Realm  string `json:"realm"`
	Domain string `json:"domain"`
	Slug   string `json:"slug"`
}

type GetArticleResponse struct {
	Articles *Article `json:"article"`
	Err      string   `json:"err,omitempty"`
}
