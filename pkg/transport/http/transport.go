package http

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-kit/kit/endpoint"
	blogonext "github.com/revas/blogo-next/pkg"
)

func MakeListArticlesEndpoint(svc blogonext.ArticlesService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(blogonext.ListArticlesRequest)
		v, err := svc.ListArticles(req.Realm, req.Domain, req.Offset, req.Limit)
		if err != nil {
			return blogonext.ListArticlesResponse{v, err.Error()}, nil
		}
		return blogonext.ListArticlesResponse{v, ""}, nil
	}
}

func DecodeListArticlesRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request blogonext.ListArticlesRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func MakeGetArticleEndpoint(svc blogonext.ArticlesService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(blogonext.GetArticleRequest)
		v, err := svc.GetArticle(req.Realm, req.Domain, req.Slug)
		if err != nil {
			return blogonext.GetArticleResponse{v, err.Error()}, nil
		}
		return blogonext.GetArticleResponse{v, ""}, nil
	}
}

func DecodeGetArticleRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request blogonext.GetArticleRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func EncodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}
