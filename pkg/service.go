package blogonext

type Content struct {
	Markdown  string `json:"markdown"`
	Hypertext string `json:"hypertext"`
	Language  string `json:"language"`
}

type Article struct {
	Slug      string   `json:"slug"`
	Title     string   `json:"title"`
	Image     string   `json:"image"`
	Thumbnail string   `json:"thumbnail"`
	Content   *Content `json:"content,omitempty"`
}

// ArticlesService provides access to published articles
type ArticlesService interface {
	ListArticles(string, string, int, int) ([]*Article, error)
	GetArticle(string, string, string) (*Article, error)
}
