package datastore

import (
	"errors"

	"github.com/go-kit/kit/log"

	blogonext "github.com/revas/blogo-next/pkg"
)

type ArticlesService struct {
	Logger log.Logger
}

func (s ArticlesService) ListArticles(Realm string, Domain string, offset int, limit int) ([]*blogonext.Article, error) {
	return nil, errors.New("Not implemented")
}

func (s ArticlesService) GetArticle(Realm string, Domain string, Slug string) (*blogonext.Article, error) {
	return nil, errors.New("Not implemented")
}
