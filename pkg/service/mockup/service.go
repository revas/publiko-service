package datastore

import (
	"fmt"

	"github.com/go-kit/kit/log"

	blogonext "github.com/revas/blogo-next/pkg"
)

type ArticlesService struct {
	Logger log.Logger
}

func (s ArticlesService) ListArticles(Realm string, Domain string, Offset int, Limit int) ([]*blogonext.Article, error) {
	articles := make([]*blogonext.Article, Limit)
	for i := 0; i < Limit; i++ {
		articles[i] = generateMockupArticle(Realm, Domain, Offset+i, true)
	}
	return articles, nil
}

func (s ArticlesService) GetArticle(Realm string, Domain string, Slug string) (*blogonext.Article, error) {
	return generateMockupArticle(Realm, Domain, 1, false), nil
}

func generateMockupArticle(Realm string, Domain string, Id int, OmitContent bool) (article *blogonext.Article) {
	article = &blogonext.Article{}
	article.Slug = fmt.Sprintf("%s-%s-%d-slug", Realm, Domain, Id)
	article.Title = fmt.Sprintf("Article %d of realm %s and domain %s", Id, Realm, Domain)
	article.Image = fmt.Sprintf("%s/%s/article-%d/header.png", Realm, Domain, Id)
	article.Thumbnail = fmt.Sprintf("%s/%s/article-%d/thumbnail.png", Realm, Domain, Id)
	if OmitContent {
		return
	}
	article.Content = &blogonext.Content{}
	article.Content.Markdown = `# Amictus redit magna

## Satiatur alia quoniam ereptaque quem Iubam

Lorem markdownum. Qui non attonitus, nosset Hippodamas parte, det in infundere?
Caeli [senectus rituque](http://www.patervis.io/coloni) geminis; concita frigora
posuistis simul aures. Tactis tonitrumque spicea nympha, illa ubi Dicta soliti
patientia ferarum. Inania faciat nomine plausis repulsam cuius et pede exhalat
metuenti nitentia *nescia merui vulnus*, nomen mutatur sustinet pater.

Ut nymphe et ac pestifero mutata pervenit Pylios captare dentes nec Hactenus
qua. Clara oraque, dixit pulsant gratia, perquirere ab artis prodesse palmis
paulatim dedere. Pugnat urbis Bacchus modo incerta, nati horruit patris. Ante
mori iacent, Pergus quam; erit etiamnum qui adulter casus dilecte quot lacertis
obstupuere tantus se.

## Fasque folioque lilia est durat saepe varios

Se imis consedit plangentibus, **Cancrum rubet raptam** nomen: adsensere
proculcat? Relinquunt monte; ambustique non numen primum cum *nobis* oris venis
pervia.

- Veluti dataque dum innitens saxea
- Mora parte noctes
- Alius ignis
- Candentia suarum exclamatque sacri
- Tutae mutataque auster oculi nemo`
	article.Content.Hypertext = `<h1 id="amictusreditmagna">Amictus redit magna</h1>

<p>## Satiatur alia quoniam ereptaque quem Iubam</p>

<p>Lorem markdownum. Qui non attonitus, nosset Hippodamas parte, det in infundere?
  Caeli <a href="http://www.patervis.io/coloni">senectus rituque</a> geminis; concita frigora
  posuistis simul aures. Tactis tonitrumque spicea nympha, illa ubi Dicta soliti
  patientia ferarum. Inania faciat nomine plausis repulsam cuius et pede exhalat
  metuenti nitentia <em>nescia merui vulnus</em>, nomen mutatur sustinet pater.</p>

<p>Ut nymphe et ac pestifero mutata pervenit Pylios captare dentes nec Hactenus
  qua. Clara oraque, dixit pulsant gratia, perquirere ab artis prodesse palmis
  paulatim dedere. Pugnat urbis Bacchus modo incerta, nati horruit patris. Ante
  mori iacent, Pergus quam; erit etiamnum qui adulter casus dilecte quot lacertis
  obstupuere tantus se.</p>

<p>## Fasque folioque lilia est durat saepe varios</p>

<p>Se imis consedit plangentibus, <strong>Cancrum rubet raptam</strong> nomen: adsensere
  proculcat? Relinquunt monte; ambustique non numen primum cum <em>nobis</em> oris venis
  pervia.</p>

<ul>
<li>Veluti dataque dum innitens saxea</li>

<li>Mora parte noctes</li>`
	return
}
